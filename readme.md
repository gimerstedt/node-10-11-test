# sort
apparently changed in v8 v7.0:  
https://v8.dev/blog/array-sort

```
$ make 10; and make 11
docker build -t node10-test -f Dockerfile.10 .
Sending build context to Docker daemon  61.44kB
Step 1/3 : from node:10-alpine
 ---> 7ca2f9cb5536
Step 2/3 : add test.js /
 ---> Using cache
 ---> a6fd7da9737d
Step 3/3 : cmd node /test.js
 ---> Using cache
 ---> 15f58fc4c30d
Successfully built 15f58fc4c30d
Successfully tagged node10-test:latest
docker run -it --rm node10-test
[ 2, 1 ]
[ 1, 2 ]
[ 1, 2 ]
docker build -t node11-test -f Dockerfile.11 .
Sending build context to Docker daemon  61.44kB
Step 1/3 : from node:11-alpine
 ---> 5d526f8ba00b
Step 2/3 : add test.js /
 ---> Using cache
 ---> f145ad00bbbb
Step 3/3 : cmd node /test.js
 ---> Using cache
 ---> 30ed381812d9
Successfully built 30ed381812d9
Successfully tagged node11-test:latest
docker run -it --rm node11-test
[ 1, 2 ]
[ 1, 2 ]
[ 2, 1 ]
```
