const comperatorA = (a, b) => {
  console.log('returning 1 from comperatorA')
  return 1;
};

const comperatorB = (a, b) => {
  if (a > b) {
    console.log('returning -1 from comperatorB');
    return -1;
  }
  if (a < b) {
    console.log('returning 1 from comperatorB');
    return 1;
  }
  return 0;
};

console.log([1, 2].sort(comperatorA));
console.log([1, 2].sort(comperatorB));
