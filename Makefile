.PHONY: 10
10:
	docker build -t node10-test -f Dockerfile.10 .
	docker run -it --rm node10-test

.PHONY: 11
11:
	docker build -t node11-test -f Dockerfile.11 .
	docker run -it --rm node11-test
